#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <unistd.h>

using namespace std;

void read_file(int fd[2], char *);
int fetch_roll(char *src, long &roll, int);

inline void err(string s, int exitCode = 0) {
	cerr << s << endl;
	if (0 != exitCode) exit(0);
}

inline void open_pipe(int fd[2]) {
	if (-1 == pipe(fd)) err("Error creating a pipe", -5);
}

inline int get_stream(char *s) {
	return (s[2] - '0') * 10 + s[3] - '0';
}

inline char *format(char *s) {
	int i = 0;
	for (; s[i] != ','; ++i);
	s[i] = ' ';
	for (; s[i] != ','; ++i);
	s[i] = '\0';
	return s;
}

int main(int argc, char *argv[]) {
	int pfd1[2], pfd2[2], rdc1, rdc2, i, j, p, q;
	long roll1, roll2;
	char buf1[512], buf2[512];
	ofstream btech("btech.txt"), mtech("mtech.txt"), phd("phd.txt"), error("error.txt");
	
	if (argc < 3) {
		cout << "Usage: " << argv[0] << " <filename1> <filename2> " << endl;
		exit(1);
	}
	
	open_pipe(pfd1);
	open_pipe(pfd2);
	
	switch(fork()) {
		case -1: err("Error forking process", -6);
		case 0: read_file(pfd1, argv[1]); break;
	}
	
	switch(fork()) {
		case -1: err("Error forking process", -6);
		case 0: read_file(pfd2, argv[2]); break;
	}
	
	if (-1 == close(pfd1[1]))  err("Error closing fd", -4);
	if (-1 == close(pfd2[1]))  err("Error closing fd", -4);
	rdc1 = rdc2 = i = j = p = q = 0;
	while (true) {
		rdc1 = i + read(pfd1[0], buf1 + i, 511 - i);
		rdc2 = j + read(pfd2[0], buf2 + j, 511 - j);
		
		if (rdc1 > 0 && 0 < rdc2) {
			for (i = 0, j = 0, roll1 = -1, roll2 = -1; i < rdc1 && j < rdc2; ) {
				// Fetch new roll from pipe 1 buf if required
				if (roll1 == -1) {
					p = fetch_roll(buf1 + i, roll1, rdc1 - i);
				}
				
				// Fetch new roll from pipe 2 buf if required
				if (roll2 == -1) {
					q = fetch_roll(buf2 + j, roll2, rdc2 - j);
				}
				
				if(roll1 < 0 || roll2 < 0) {
					break;
				} else if (roll1 > roll2) {
					roll2 = -1;
					j += q;
				} else if (roll1 < roll2) {
					roll1 = -1;
					i += p;
				} else {
					// Intersection write to file	
					switch(get_stream(buf1 + i)) {
						case 1: btech << format(buf1 + i) << endl; break;
						case 41: mtech << format(buf1 + i) << endl; break;
						case 61:
						case 62: phd << format(buf1 + i) << endl; break;
						default: error << format(buf1 + i) << endl; break;
					}
					
					roll1 = -1;
					roll2 = -1;
					i += p;
					j += q;
				}
				
			}
			strcpy(buf1, buf1 + i);
			strcpy(buf2, buf2 + j);
			i = rdc1 - i;
			j = rdc2 - j;
		} else {
			cout << "-----------------------------" << rdc1 << "  " << rdc2 << endl;
			break;
		};
	}
	
	if (-1 == close(pfd1[0]))  err("Error closing fd", -1);
	if (-1 == close(pfd2[0]))  err("Error closing fd", -1);
	btech.close(); mtech.close(); phd.close();
	return 0;
}

void read_file(int fd[2], char *filename) {
	char buf[513] ;

	// Closing read end of pipe
	if (-1 == close(fd[0]))  err("Error closing fd", -1);
	
	ifstream fin(filename);
	
	if (!fin.good()) err("Error opening file: `" + string(filename) + "`", -2);
	
	while(fin.good()) {
		fin.read(buf, 512);
		write(fd[1], buf, 512);
		if (fin.eof()) {
			break;
		}
	}
	
	cout << "Closing " << filename << endl;
	fin.close();
	
	if (-1 == close(fd[1])) err("Error closing fd", -4);
	
	_exit(EXIT_SUCCESS);
}

int fetch_roll(char *src, long &roll, int boundary) {
	int i = 0;
	roll = 0;

	for (; src[i] != ',' && i < boundary; ++i) {
		if (!isdigit(src[i])) break;
		roll = roll * 10 + src[i] - '0';
	}
	
	for (; (src[i] != '\n' || src[i] != '\0') && i < boundary; ++i);
	
	if (i >= boundary ) {
		roll = -1;
		return 0;
	}
	
	return i + 1;
}
