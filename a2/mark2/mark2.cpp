#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <unistd.h>
#include <algorithm>

using namespace std;

inline void err(string s, int exitCode = 0) {
	cerr << s << endl;
	if (0 != exitCode) exit(0);
}

inline void open_pipe(int fd[2]) {
	if (-1 == pipe(fd)) err("Error creating a pipe", -5);
}

inline int get_stream(char *s) {
	return (s[2] - '0') * 10 + s[3] - '0';
}

inline char *format(char *s) {
	int i = 0;
	for (; s[i] != ','; ++i);
	s[i] = ' ';
	for (; s[i] != ','; ++i);
	s[i] = '\0';
	return s;
}

int main(int argc, char *argv[]) {
	int pfd1[2], pfd2[2], rdc1, rdc2;
	long roll1, roll2;
	char buf1[512], buf2[512];
	
	if (argc < 3) {
		cout << "Usage: " << argv[0] << " <filename1> <filename2> " << endl;
		exit(1);
	}

	ofstream btech("btech.txt"), mtech("mtech.txt"), phd("phd.txt"), error("error.txt");
	ifstream file1(argv[1]), file2(argv[2]);
	
	open_pipe(pfd1);
	open_pipe(pfd2);
	
	while (true) {
		file1.getline(buf1, 512);
		if (file1.eof()) break;
		write (pfd1[1], buf1, strlen(buf1)+1);

		if (0 != read(pfd1[0], buf1, 512)) {
			file2.clear(0);
			file2.seekg(0, file2.beg);
			while (true) {
				file2.getline(buf2, 512);
				if (file2.eof()) break;
				write(pfd2[1], buf2, strlen(buf2)+1);

				if (0 != read(pfd2[0], buf2, 512)) {
					if (0 == strcmp(buf1, buf2)) {
						switch(get_stream(buf1)) {
							case 1: btech << format(buf1) << endl; break;
							case 41: mtech << format(buf1) << endl; break;
							case 61:
							case 62: phd << format(buf1) << endl; break;
							default: error << format(buf1) << endl; break;
						}
						cout << "Found: " << format(buf2) << endl;
					}
				} else break;
			}
		} else break;
	}
	
	btech.close(); mtech.close(); phd.close();
	return 0;
}