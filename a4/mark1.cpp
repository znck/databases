#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <vector>
#include <cerrno>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <cctype>
#include <map>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>

#define debug true
#define ifDebug(a) if(debug) {a}

using namespace std;

inline bool isFile(const char *);
inline bool isDir(const char *);
void load_courses(const char *);
void load_students(const char *);
void print_students(const char *);
void run_child(const char *);

map<string, vector<string> > students;
map<string, string> courses;

int main(int argc, char * argv[]) {
	string root_file = "database";
	if (argc > 1) {
		root_file = string(argv[1]);
	}
	load_courses((root_file + "/index.txt").c_str());
	load_students(root_file.c_str());
	run_child(root_file.c_str());
	return 0;
}

int getIndex(string s) {
	int z = 0;
	if (isdigit(s[0])) {
		z = (s[4] - '0') * 10 + s[5] - '0';
	} else {
		cerr << "Invalid roll: " << s << endl;
		z = (s[5] - '0') * 10 + s[6] - '0';
	}
	if ((0 < z && z < 9) || (20 < z && z < 24) || (41 == z)) {
		return z;
	}
	return 0;
}
string to_upper(string in) {
	for (int i = 0; i < in.length(); ++i) {
		in[i] = toupper(in[i]);
	}
	return in;
}
void print_students(const char * file) {
	char *buffer;
	int fd = open(file, O_RDWR);
	struct stat sb;
	fstat(fd, &sb);
	buffer = (char *)mmap(NULL, sb.st_size, PROT_WRITE, MAP_SHARED, fd, 0);
	if (buffer == MAP_FAILED) {
		cerr << "Map Failed" << endl;
		exit(-1);
	}
	close(fd);
	switch(fork()) {
		case -1: cerr << "Error: cannot fork" << endl;
			 exit(-1);
			 break;
		case 0:  string out, roll;
			 cout << "File:: " << file << endl;
			 for (int i = 0, j; i < sb.st_size; ++i) {
				 for (j = i; j < sb.st_size; ++j) {
					 if (',' == buffer[j]) {	 		
						 break;
					 }
				 }
				 roll = string(buffer + i, j - i);
				 out.append(roll);
				 map<string, vector<string> >::iterator r = students.find(roll);
				 if (r != students.end()) {
					 for (vector<string>::iterator c = r->second.begin(); c != r->second.end(); ++c) {
						 out.append(",").append(*c);
					 }
				 }
				 out.append("\n");
				 while('\n' != buffer[i] && i < sb.st_size) {
					 ++i;
				 }
			 }
			 if (msync(buffer, sb.st_size, MS_SYNC) == -1) {
				 cerr << "Error map sync" << endl;
				 exit(-1);
			 }
			 strncpy(buffer, to_upper(out).c_str(), sb.st_size);
			 exit(0);
			 break;
	}
	wait(NULL);
	munmap(buffer, sb.st_size);
}

void run_child(const char *path) {
	DIR *dirp = opendir(path);
	const dirent *dp;
	if (NULL == dirp) {
		cerr << "Error opening `" << path << "`" << endl;
		return;
	}

	errno = 0;
	string t;
	while ((dp = readdir(dirp))) {
		if (NULL == dp) break;

		if (
				0 == strcmp(dp->d_name, ".") ||
				0 == strcmp(dp->d_name, "..") ||
				0 == strcmp(dp->d_name, "index.txt") 
		   ) continue;

		t = string(path).append("/").append(dp->d_name);
		if (isDir(t.c_str())) run_child(t.c_str()); 
		else print_students(t.c_str());
	}
}

void load_courses(const char *path) {
	assert (isFile(path));

	ifstream in(path);
	char input[101];
	size_t e;
	int count = 0;

	while(in.good()) {
		++count;
		in.getline(input, 100);

		string s(input);
		if(s.length() == 0) continue;

		e = s.find(',');

		if (e == string::npos) {
			cerr << "Skiping:" << count << ": " << e << endl;
			continue;
		}

		ifDebug(
				// cerr << s << endl;
		       );

		courses.insert(
				pair<string, string>(s.substr(0, e), s.substr(e + 1, s.length()))
			      );

		memset(input, 0, 101);
	}
}

void iterate_dir(const char *path) {
	DIR *dirp = opendir(path);
	const dirent *dp;
	if (NULL == dirp) {
		cerr << "Error opening `" << path << "`" << endl;
		return;
	}

	errno = 0;
	while ((dp = readdir(dirp))) {
		if (NULL == dp) break;

		if (
				0 == strcmp(dp->d_name, ".") ||
				0 == strcmp(dp->d_name, "..") ||
				0 == strcmp(dp->d_name, "index.txt") 
		   ) continue;

		load_students(string(path).append("/").append(dp->d_name).c_str());
	}

	if (errno != 0) {
		cerr << "Error reading `" << path << "`" << endl;
		return;
	}
}

void load_students(const char *path) {
	if (isDir(path)) {
		iterate_dir(path);
		return;
	} else {
		ifstream is(path);
		char buf[201];
		size_t e = string(path).find_last_of('/');
		string c = string(path).substr(e+1, string(path).find_last_of('.') - e - 1);
		ifDebug(
				// cerr << "course: " << c << endl;
		       );
		while (is.good()) {
			is.getline(buf, 200);
			string line(buf);
			if (line.length() == 0) continue;

			ifDebug(
					// cerr << "    " << line << endl;
			       );

			e = line.find(',');
			if (e == string::npos) {
				cerr << "Error: " << line << endl;
				continue;
			}

			string roll = line.substr(0, e);

			map<string, vector<string> >::iterator stud = students.find(roll);
			if (stud == students.end()) {
				vector<string> *t = new vector<string>;
				t->push_back(c);
				students.insert(make_pair(roll, *t));
			} else {
				stud->second.push_back(c);
			}
		}
	}
}

inline bool isFile(const char *p) {
	struct stat statbuf;
	return stat(p, &statbuf) == 0 && (statbuf.st_mode & S_IFMT) == S_IFREG;
}

inline bool isDir(const char *p) {
	struct stat statbuf;
	return stat(p, &statbuf) == 0 && (statbuf.st_mode & S_IFMT) == S_IFDIR;
}
