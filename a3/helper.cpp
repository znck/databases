#include <iostream>
#include <cstring>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdlib>
#include <fstream>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <cerrno>

using namespace std;

int main(int argc, char const *argv[])
{
	int fd;
	if (argc < 3){
	 	cerr << "Usage: " << argv[0] << " <fifo> <filename>" << endl;
	 	exit(1);
	}

	fd =  open(argv[1], O_WRONLY);
	if (fd == -1) {
		cerr << "Error in opening " << argv[1] << " in write mode" << endl;
		exit(-1);
	}

	ifstream in(argv[2]);

	if (!in.good()) {
		cerr << "error in opening file" << argv[2] << endl;
		exit(2);
	}

	char buf[100];

	int i = 0;
	while(in.good()) {
		memset(buf, 0, 100);
		in.getline(buf, 100);
		if (in.eof()) break;
		
		if (strlen(buf) == 0) continue;
		// cout << ++i << ":: " << buf << endl;
		write(fd, buf, 100);
	}
	cerr << "DEBUG::finish:: Fill fifo from file" << endl;
	
	in.close();
	close(fd);
	
	return 0;
}
