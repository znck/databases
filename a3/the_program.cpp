#include <iostream>
#include <cstring>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <cerrno>

using namespace std;

#define TEMP_FIFO "interm_fifo"


long parse(const char *str) {
	long roll = 0, len = strlen(str);
	for (int i = 0; i < len; ++i) {
		if (str[i] >= '0' && str[i] <= '9') {
			roll = roll * 10 + str[i] - '0';
		} else break;
	}
	return roll;
}

int main(int argc, char const *argv[])
{
	int fd1, fd2, fd3;
	if (argc < 2){
	 	cerr << "Usage: " << argv[0] << " <fifo> " << endl;
	 	exit(1);
	}

	fd1 =  open(argv[1], O_RDONLY);
	if (fd1 == -1) {
		cerr << "Error in opening fifo `" << argv[1] << "` in read mode." << endl;
		exit(-1);
	}

	if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
		cerr << "Error in capturing SIGPIPE error" << endl;
		exit(-1);
	}
	
	umask(0);
	if(mkfifo(TEMP_FIFO, S_IRUSR | S_IWUSR | S_IWGRP) == -1 
		&& errno != EEXIST) {
		cerr << "Error creating fifo `" << TEMP_FIFO << "`." << endl;
		exit(-1);
	}
	
	fd3 = open(TEMP_FIFO, O_RDONLY | O_NONBLOCK);
	if (fd3 == -1) {
		cerr << "Error in opening fifo `" << TEMP_FIFO 
			<< "` in read mode." << endl;
		exit(-1);
	}
	
	fd2 = open(TEMP_FIFO, O_WRONLY | O_NONBLOCK);
	if (fd2 == -1) {
		cerr << "Error in opening fifo `" << TEMP_FIFO 
			<< "` in write mode." << endl;
		exit(-1);
	}
	
	char buf[100] = {0}, max[100] = {0};
	long rollmax = -2, count = 0;
	
	while (0 != read(fd1, buf, 100)) {
		write(fd2, buf, 100);
		if (strlen(buf) == 0) break;
		count++;
	}
	
	close(fd1);
	fd1 =  open(argv[1], O_WRONLY);
	if (fd1 == -1) {
		cerr << "Error in opening fifo `" << argv[1] << "` in write mode." << endl;
		exit(-1);
	}
	
	int i = 0;
	while(0 != read(fd3, buf, 100)) {
		if(parse(buf) > rollmax) {
			if (-2 != rollmax) {
				write(fd2, max, 100);
			}
			strcpy(max, buf);
			rollmax = parse(max);
		} else {
			write(fd2, buf, 100);
		}
		if (count == 0) break;
		
		++i;
		if (i > count) {
			write(fd1, max, strlen(max));
			write(fd1, "\n", 1);
			rollmax = -2;
			i = 0;
			--count;
		}
	}
	
	close(fd1);
	close(fd2);

	return 0;
}
