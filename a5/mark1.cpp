#include <iostream>
#include <cstring>
#include <cstdlib>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <cerrno>
#include <cstdio>

#define BLANK 0

using namespace std;

char* reverse(char *p, int length) {
	char *q = p;
  	while(q && *q) ++q;
  	for(--q; p < q; ++p, --q)
    *p = *p ^ *q, *q = *p ^ *q, *p = *p ^ *q;
	return p;
}

char* itoa(int num, char* str, int base)
{
    int i = 0;
    bool isNegative = false;

    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
    if (num == 0)
    {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }

    // In standard itoa(), negative numbers are handled only with
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10)
    {
        isNegative = true;
        num = -num;
    }

    // Process individual digits
    while (num != 0)
    {
        int rem = num % base;
        str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
        num = num/base;
    }

    // If number is negative, append '-'
    if (isNegative)
        str[i++] = '-';

    str[i] = '\0'; // Append string terminator

    // Reverse the string
    return reverse(str, i);
}

typedef struct {
	char pid[6];
	char status[5];
} stored_entry;

const char* codes[] = {
	"BLNK\n",
	"KZJ \n",
	"RDM \n",
	"BPQ \n",
	"NGP \n",
	"BPL \n",
	"JHS \n",
	"GWL \n",
	"AGC \n",
	"NDLS\n"
};
class entry {
private:
	char data[11];
	stored_entry se;
	unsigned int pid;
	short status;
	
	void _write() {
		strcpy(se.pid, "      ");
		itoa(pid, se.pid, 10);
		strncpy(se.pid + strlen(se.pid), "      ", 6 - strlen(se.pid));
		
		strcpy(se.status, codes[status]);
	}
	
	void _read() {
		pid = atoi(se.pid);
		
		status = 0;
		for (int i = 0; i < sizeof(codes) / sizeof(char *); ++i) {
			if (strncmp(se.status, codes[i], 4) == 0) {
				status = i;
				break;
			}
		}
	}
public:
	entry(unsigned int _pid = 0, short _status = BLANK) {
		pid = _pid;
		status = _status;
	}
	
	void set(stored_entry _se) {
		se = _se;
	}
	
	const char * get() {
		_write();
		memcpy(data, &se, sizeof(se));
		return data;
	}
	
	void set(char *_se) {
		memcpy(&se, _se, sizeof(se));
		_read();
	}
	
	void set(unsigned int _pid) {
		pid = _pid;
	}
	
	void set(short _status) {
		status = _status;
	}
	
	unsigned int get_pid() {
		return pid;
	}
	
	bool get_status() {
		return status;
	}
};

#define perr(condition,error) if (condition) { \
								cerr << "ERR("<< errno <<")::" << error << endl; \
								exit(errno); \
						      }
						    	
int launch(int id, int fd, int cnt) {
	struct flock lck;
	int block = sizeof(stored_entry),
		s = (id - 1) * block * cnt,
		len = block * cnt,
		pid = getpid();
	
	lck.l_type = F_WRLCK;
	lck.l_whence = SEEK_SET;
	lck.l_start = s;
	lck.l_len = len;
	
	printf("child #%d lock:: %3d to %3d pid:: %d records:: %d block-size:: %d\n", id, s, s+len, pid, cnt, block);
	
	int status = fcntl(fd, F_SETLKW, &lck);
	if (0 == status) {
		char *buf = new char[block];
		entry e;
		int lr;
		
		perr(-1 == lseek(fd, s, SEEK_SET), "child #" << id << " failed to seek file to " << s + i * block);
		for (int i = 0; i < cnt; ++i) {
			perr(-1 == (lr = read(fd, buf, block)), "child #" << id << " failed to read from file");
			if (lr != block) {
				lseek(fd, -block, SEEK_CUR);
			}
			e.set(buf);
			if (e.get_status() == BLANK) {
				e.set((short)id);
				e.set((unsigned int)pid);
				perr(-1 == lseek(fd, -block, SEEK_CUR), "child #" << id << " failed to seek file to " << s + i * block);
				// printf("child #%d %.11s", id, e.get());
				perr(block != write(fd, e.get(), block), "child #" << id << " failed to write to file");
			}
		}
		lck.l_type = F_UNLCK;
		fcntl(fd, F_SETLKW, &lck);
	} else perr(status == EAGAIN || status == EACCES, 
		"child #" << id << " failed to lock bytes " << s << " to " << s + len << " (incompatible lock)")
	else perr(status == EDEADLK, "child #" << id << " failed due to dead lock")
	else perr(true, "some unknown error");
	return 0;
}					      

void child(int id, int fd, int cnt) {
	switch(fork()) {
		case -1:
			perr(true, "failed to create child #" << id);
			break;
		case 0:
			exit(launch(id, fd, cnt));
			break;
		default:
			wait(NULL);
			return;
	}
}

int main(int argc, char *argv[]) {
	entry e;
	char filename[100] = "db.txt";
	int fd;
	
	// Creating emty file
	perr(-1 == (fd = open(filename, O_RDWR | O_CREAT, 0777)), "failed to open file " << filename);
	
	for (unsigned int i = 1; i <= 72; ++i) {
		e.set(i);
		perr(sizeof(stored_entry) != write(fd, e.get(), sizeof(stored_entry)), "failed to write to file " << filename);
	}
	
	for (int i = 1; i <= 9; ++i) {
		child(i, fd, 8);
	}
	
	/* // synchronizing all children 
	while (true) {
		int status;
		pid_t done = wait(&status);
		if (done == -1) {
		    if (errno == ECHILD) break; // no more child processes
		} else {
		    if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
		        cerr << "pid " << done << " failed" << endl;
		        exit(1);
		    }
		}
	}*/


	
	perr(-1 == close(fd), "error closing file " << filename);
	return 0;
}
