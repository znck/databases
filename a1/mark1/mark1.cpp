#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cerrno>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <cctype>
#include <map>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

#define debug true
#define ifDebug(a) if(debug) {a}

using namespace std;

inline bool isFile(const char *);
inline bool isDir(const char *);
void load_courses(const char *);
void load_students(const char *);
void print_students();

map<string, vector<string> > students;
map<string, string> courses;

int main(int argc, char * argv[]) {
	string root_file = "database";
	if (argc > 1) {
		root_file = string(argv[1]);
	}
	load_courses((root_file + "/index.txt").c_str());
	load_students(root_file.c_str());
	print_students();
	return 0;
}

int getIndex(string s) {
	int z = 0;
	if (isdigit(s[0])) {
		z = (s[4] - '0') * 10 + s[5] - '0';
	} else {
		cerr << "Invalid roll: " << s << endl;
		z = (s[5] - '0') * 10 + s[6] - '0';
	}
	if ((0 < z && z < 9) || (20 < z && z < 24) || (41 == z)) {
		return z;
	}
	return 0;
}

void print_students() {
	map<int, ofstream*> out;
	out[0] = new ofstream("output/error.txt");
	out[1] = new ofstream("output/cse.txt");
	out[2] = new ofstream("output/ece.txt");
	out[3] = new ofstream("output/me.txt");
	out[4] = new ofstream("output/ce.txt");
	out[5] = new ofstream("output/dd.txt");
	out[6] = new ofstream("output/bt.txt");
	out[7] = new ofstream("output/cl.txt");
	out[8] = new ofstream("output/eee.txt");
	out[21] = new ofstream("output/ep.txt");
	out[22] = new ofstream("output/ct.txt");
	out[23] = new ofstream("output/mc.txt");
	out[41] = new ofstream("output/hs.txt");

	for (map<string, vector<string> >::iterator i = students.begin(); i != students.end(); ++i) {
		string space("                    ");
		space = space.substr(0, i->first.length());
		int z = getIndex(i->first);
		*out[z] << i->first;
		ifDebug(
			if (z == 0) cerr << "Roll: " << i->first << endl;
		);
		for (vector<string>::iterator j = i->second.begin(); j != i->second.end(); ++j) {
			if (j != i->second.begin()) *out[z] << space ;
			if (z == 0) {
				cerr << "\t" << *j << "\t" << courses[*j] << endl;;
			}
			*out[z] << "\t" << *j << "\t" << courses[*j] << endl;
		}
	}
}

void load_courses(const char *path) {
    assert (isFile(path));

    ifstream in(path);
    char input[101];
    size_t e;
    int count = 0;
    
    while(in.good()) {
    	++count;
    	in.getline(input, 100);

    	string s(input);
    	if(s.length() == 0) continue;

    	e = s.find(',');

    	if (e == string::npos) {
    		cerr << "Skiping:" << count << ": " << e << endl;
    		continue;
    	}

    	ifDebug(
    		// cerr << s << endl;
    	);

    	courses.insert(
    		pair<string, string>(s.substr(0, e), s.substr(e + 1, s.length()))
    	);
        
        memset(input, 0, 101);
    }
}

void iterate_dir(const char *path) {
	DIR *dirp = opendir(path);
	const dirent *dp;
	if (NULL == dirp) {
		cerr << "Error opening `" << path << "`" << endl;
		return;
	}

	errno = 0;
	while ((dp = readdir(dirp))) {
		if (NULL == dp) break;

		if (
			0 == strcmp(dp->d_name, ".") ||
			0 == strcmp(dp->d_name, "..") ||
			0 == strcmp(dp->d_name, "index.txt") 
		) continue;

		load_students(string(path).append("/").append(dp->d_name).c_str());
	}

	if (errno != 0) {
		cerr << "Error reading `" << path << "`" << endl;
		return;
	}
}

void load_students(const char *path) {
	if (isDir(path)) {
		iterate_dir(path);
		return;
	} else {
		ifstream is(path);
		char buf[201];
		size_t e = string(path).find_last_of('/');
		string c = string(path).substr(e+1, string(path).find_last_of('.') - e - 1);
		ifDebug(
			// cerr << "course: " << c << endl;
		);
		while (is.good()) {
			is.getline(buf, 200);
			string line(buf);
			if (line.length() == 0) continue;

			ifDebug(
				// cerr << "    " << line << endl;
			);

			e = line.find(',');
			if (e == string::npos) {
				cerr << "Error: " << line << endl;
				continue;
			}

			string roll = line.substr(0, e);

			map<string, vector<string> >::iterator stud = students.find(roll);
			if (stud == students.end()) {
				vector<string> *t = new vector<string>;
				t->push_back(c);
				students.insert(make_pair(roll, *t));
			} else {
				stud->second.push_back(c);
			}
		}
	}
}

inline bool isFile(const char *p) {
    struct stat statbuf;
    return stat(p, &statbuf) == 0 && (statbuf.st_mode & S_IFMT) == S_IFREG;
}

inline bool isDir(const char *p) {
    struct stat statbuf;
    return stat(p, &statbuf) == 0 && (statbuf.st_mode & S_IFMT) == S_IFDIR;
}